import { preloadFonts } from './src/utils/utils'
import { Intro } from './intro'
import { gsap } from 'gsap'
import {ScrollTrigger} from 'gsap/ScrollTrigger'
import { TextPlugin } from "gsap/TextPlugin"
import { Observer } from "gsap/Observer"
import { Draggable } from "gsap/Draggable"
import { Flip } from "gsap/Flip"
import { ScrollToPlugin } from "gsap/ScrollToPlugin"

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin, Observer, Draggable, TextPlugin, Flip);

const intro = new Intro(document.querySelector('.circles'));

// Preload images and fonts
Promise.all([preloadFonts('kxo3pgz')]).then(() => {
    // remove loader (loading class)
    document.body.classList.remove('loading');
    // start intro
    intro.start();
});

var tl = gsap.timeline();
var duration = 2;
function init() {
	tl.fromTo("h1", 
		{ scale: 0 },
		{ 
    duration:duration,
		scale: 6,
		ease: "slow(0.4, 0.9)",
		stagger: 1.5
		}
	)
	.from("h1",  
		  {
    duration:duration,
		opacity:0,
		ease:"slow(0.4, 0.9, true)",
		stagger: 1.5
  }, 
  "<");
}

window.addEventListener("load", function(event) {
	console.log("load");
	gsap.set(".box", { autoAlpha: 1 });
	init();
});
